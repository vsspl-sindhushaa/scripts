package transaction.script.models;

import java.util.HashSet;

public class DomainTo {
	private String bizName;
	private String email;
	private HashSet<String> googlelinks;
	private String domainName;
	private String domainHitCount;
	private boolean isValidDomain;
	private String userCount;
	private String postCount;
	private boolean isGlobal;
	private String countryNames;

	public String getBizName() {
		return bizName;
	}

	public void setBizName(String bizName) {
		this.bizName = bizName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public HashSet<String> getGooglelinks() {
		return googlelinks;
	}

	public void setGooglelinks(HashSet<String> googlelinks) {
		this.googlelinks = googlelinks;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getDomainHitCount() {
		return domainHitCount;
	}

	public void setDomainHitCount(String domainHitCount) {
		this.domainHitCount = domainHitCount;
	}

	public boolean isValidDomain() {
		return isValidDomain;
	}

	public void setValidDomain(boolean isValidDomain) {
		this.isValidDomain = isValidDomain;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	public String getPostCount() {
		return postCount;
	}

	public void setPostCount(String postCount) {
		this.postCount = postCount;
	}

	public boolean isGlobal() {
		return isGlobal;
	}

	public void setGlobal(boolean isGlobal) {
		this.isGlobal = isGlobal;
	}

	public String getCountryNames() {
		return countryNames;
	}

	public void setCountryNames(String countryNames) {
		this.countryNames = countryNames;
	}

}
