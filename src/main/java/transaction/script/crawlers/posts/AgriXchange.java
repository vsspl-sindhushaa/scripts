package transaction.script.crawlers.posts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import transaction.script.constants.AppConstants;

public class AgriXchange {
	static String viewstate = null;
	static String eventValidation = null;
	static boolean nextPageExists = false;
	static int rowNumber = 0;

	public static void main(String[] args) {
		try {
			Response response = Jsoup.connect("http://agriexchange.apeda.gov.in/tradeleads/buysellarchives.aspx?buysell=buy").timeout(60000).execute();
			Document initialDocument = response.parse();
			Map<String, String> cookie = response.cookies();
			Workbook workbook = new XSSFWorkbook();
			Sheet sheet = workbook.createSheet(AppConstants.DATA_SHEET);
			extractDetailsAndInsertIntoSheet(initialDocument, workbook, sheet);
			while (nextPageExists) {
				try {
					TimeUnit.SECONDS.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Document document = Jsoup.connect("http://agriexchange.apeda.gov.in/tradeleads/buysellarchives.aspx?buysell=buy").data("__EVENTTARGET", "DL_BuySell")
						.data("__EVENTARGUMENT", "Page$Next").data("__VIEWSTATE", viewstate).data("__EVENTVALIDATION", eventValidation).cookies(cookie).timeout(60000).post();
				FileInputStream fileInputStream = new FileInputStream("/home/abhi/Desktop/postsbuy.xlsx");
				workbook = new XSSFWorkbook(fileInputStream);
				sheet = workbook.getSheet(AppConstants.DATA_SHEET);
				extractDetailsAndInsertIntoSheet(document, workbook, sheet);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static boolean extractDetailsAndInsertIntoSheet(Document document, Workbook workbook, Sheet sheet) {
		viewstate = document.getElementById("__VIEWSTATE").attr("value");
		eventValidation = document.getElementById("__EVENTVALIDATION").attr("value");
		nextPageExists = document.toString().contains("../images/next-on.gif");
		Elements elements = document.getElementById("DL_BuySell").getElementsByTag("tr");
		int count = 0;
		String description = "", bizName = "", location = "", address = "", email = "", createdDate = "", expiryDate = "", phone = "", country = "";
		for (int i = 1, j = 0;; i++, j++) {
			Element currentElement = elements.get(i);
			if (j == 0) {
				description = "";
				bizName = "";
				location = "";
				address = "";
				email = "";
				createdDate = "";
				expiryDate = "";
				phone = "";
				country = "";
				continue;
			}
			if (j == 1) {
				Elements subElements = currentElement.getElementsByTag("td");
				if (subElements.get(0).text().contains("Description")) {
					description = subElements.get(2).text();
				}
			}
			if (j == 2) {
				Elements subElements = currentElement.getElementsByTag("tbody").get(0).getElementsByTag("tr");
				for (Node node : currentElement.getElementsByTag("tbody").get(0).childNodes()) {
					if (node.nodeName().equals("#comment")) {
						String temp = Jsoup.parse(node.toString().replace("<!-- ", "").replace("-->", "")).text();
						phone = temp.substring(temp.indexOf(":-") + 2).trim();
					}
				}
				for (Element element : subElements) {
					if (element.getElementsByTag("td").get(0).text().contains("Company Name")) {
						String data = element.getElementsByTag("td").get(2).text();
						String temp[] = data.split(",");
						bizName = temp[0].trim();
						location = data.substring(data.indexOf(",") + 1).trim();
						if (temp.length > 0)
							country = temp[temp.length - 1].trim();
					}
					if (element.getElementsByTag("td").get(0).text().contains("Address")) {
						address = element.getElementsByTag("td").get(2).text();
					}
					if (element.getElementsByTag("td").get(0).text().contains("EmailID")) {
						email = element.getElementsByTag("td").get(2).text();
					}
					if (element.getElementsByTag("td").get(0).text().contains("Posted Date")) {
						createdDate = element.getElementsByTag("td").get(2).text();
					}
					if (element.getElementsByTag("td").get(0).text().contains("Expiry Date")) {
						expiryDate = element.getElementsByTag("td").get(2).text();
					}
				}
			}
			if (currentElement.text().contains("Click Here for more Information..")) {
				if (count == 0)
					count++;
				else {
					System.out.println(description);
					System.out.println(bizName);
					System.out.println(country);
					System.out.println(location);
					System.out.println(address);
					System.out.println(email);
					System.out.println(createdDate);
					System.out.println(expiryDate);
					System.out.println(phone);
					Row row = sheet.createRow(rowNumber);
					rowNumber++;
					row.createCell(0).setCellValue(phone);
					row.createCell(1).setCellValue(bizName);
					row.createCell(2).setCellValue(location);
					row.createCell(3).setCellValue(country);
					row.createCell(4).setCellValue(address);
					row.createCell(5).setCellValue(email);
					row.createCell(6).setCellValue(description);
					row.createCell(7).setCellValue(createdDate);
					row.createCell(8).setCellValue(expiryDate);
					System.out.println("\n\n");
					j = -1;
					count = 0;
				}
			}
			if (currentElement.toString().contains("javascript:")) {
				if (workbook != null) {
					try {
						FileOutputStream outputStream = new FileOutputStream("/home/abhi/Desktop/postsbuy.xlsx");
						workbook.write(outputStream);
						/*if (rowNumber >= 30) {
							System.exit(0);
						}*/
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception exception) {
						exception.printStackTrace();
					}
				}
				break;
			}
		}
		return false;
	}
}
