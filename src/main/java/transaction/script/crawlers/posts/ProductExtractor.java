package transaction.script.crawlers.posts;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.platform.utils.HsqlDBUtil;

public class ProductExtractor {
	public static final Logger LOGGER = LoggerFactory.getLogger(ProductExtractor.class);
	public static final List<String> delimiters = Collections.unmodifiableList(Arrays.asList(new String[] { ".", "@", "!", "#", "$", "%", "&", "~", "^", "*", "+", "-", "/", "\\",
			":", ";", "'", "\"", "[", "]", "(", ")", "{", "}", "=", ",", "_", "various", "preparation", "standard" }));
	public static final List<String> ignoredProducts = Collections.unmodifiableList(Arrays.asList(new String[] { "rape", "coco", "wal", "ney", "usa", "cul", "nel", "andi", "bel",
			"ananas", "red", "ram", "ita", "ari", "jab", "oon", "ale" }));
	static HashMap<String, String> baseProducts = new HashMap<String, String>();
	static HashMap<String, ArrayList<String>> qualifiers = new HashMap<String, ArrayList<String>>();
	static ArrayList<String> countries = new ArrayList<String>();
	static Connection hsqlConnection = null;
	static ProductExtractor classObj = new ProductExtractor();
	static {
		hsqlConnection = HsqlDBUtil.dbConnection;
		classObj.getAllBaseProdcuts();
		baseProducts.put("guargum", "000000017920");
		baseProducts.put("sesame seed", "000000019540");
		baseProducts.put("ground nut", "000000004590");
		baseProducts.put("chilly", "000000000638");
		baseProducts.put("grape", "000000003997");
		baseProducts.put("cashew nut", "000000000049");
		baseProducts.put("egg", "000000003032");
	}

	public static void main(String[] args) {
		try {
			Workbook workbook = new XSSFWorkbook(new FileInputStream("/home/abhi/Desktop/sellposts-agrixchange.xlsx"));
			Sheet sheet = workbook.getSheet(AppConstants.DATA_SHEET);
			for (Row row : sheet) {
				System.out.println(row.getRowNum());
				ArrayList<String> data = classObj.extractProductsFromData(row.getCell(5).getStringCellValue());
				row.createCell(8).setCellValue(data.get(0));
				row.createCell(9).setCellValue(data.get(1));
				row.createCell(10).setCellValue(data.get(2));
			}
			workbook.write(new FileOutputStream("/home/abhi/Desktop/sellposts-agrixchange.xlsx"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<String> extractProductsFromData(String data) {
		ArrayList<String> list = new ArrayList<String>();
		String products = "";
		String productIds = "";
		String qualifier = "";
		for (String delimiter : delimiters) {
			if (data.toLowerCase().contains(delimiter))
				data = data.toLowerCase().replace(delimiter.toString(), " ");
		}
		data = classObj.removeAllCountryNames(data);
		for (String product : baseProducts.keySet()) {
			if (ignoredProducts.contains(product))
				continue;
			if (data.toLowerCase().contains(product)) {
				boolean skip = false;
				for (String temp : data.split(" ")) {
					if (temp.contains(product)) {
						if (temp.length() > product.length() + 2)
							skip = true;
					}
				}
				if (skip)
					continue;
				if (products.isEmpty()) {
					products = product;
					String prodcutId = baseProducts.get(product);
					productIds = getProductsMappedToIds(productIds, product);
					String extractedQualifier = classObj.extractQualifiers(data, qualifiers.get(prodcutId));
					if (extractedQualifier != null)
						qualifier = prodcutId + ":" + extractedQualifier;
				} else {
					products = products + "," + product;
					String prodcutId = baseProducts.get(product);
					productIds = getProductsMappedToIds(productIds, product);
					String extractedQualifier = classObj.extractQualifiers(data, qualifiers.get(prodcutId));
					if (extractedQualifier != null)
						qualifier = qualifier + ";" + prodcutId + ":" + extractedQualifier;
				}
			}
		}
		if (products.isEmpty()) {
			for (String word : data.split(" ")) {
				if (ignoredProducts.contains(word.toLowerCase().trim()))
					continue;
				if (baseProducts.containsKey(word.toLowerCase().trim())) {
					if (products.isEmpty()) {
						products = word;
						String prodcutId = baseProducts.get(word.toLowerCase().trim());
						productIds = getProductsMappedToIds(productIds, word.toLowerCase().trim());
						String extractedQualifier = classObj.extractQualifiers(data, qualifiers.get(prodcutId));
						if (extractedQualifier != null)
							qualifier = prodcutId + ":" + extractedQualifier;
					} else {
						products = products + "," + word;
						String prodcutId = baseProducts.get(word.toLowerCase().trim());
						productIds = getProductsMappedToIds(productIds, word.toLowerCase().trim());
						String extractedQualifier = classObj.extractQualifiers(data, qualifiers.get(prodcutId));
						if (extractedQualifier != null)
							qualifier = qualifier + ";" + prodcutId + ":" + extractedQualifier;
					}
				}
			}
		}
		if (data.toLowerCase().contains("vegetable")) {
			if (products.isEmpty()) {
				products = "leafy vegetables,potato,tomato,carrot";
			} else {
				products = products + ",leafy vegetables,potato,tomato,carrot";
			}
			productIds = getProductsMappedToIds(productIds, "leafy vegetables,potato,tomato,carrot");
		}
		if (data.toLowerCase().contains("fruit")) {
			if (products.isEmpty()) {
				products = "apple,banana,mango,grapes";
			} else {
				products = products + ",apple,banana,mango,grapes";
			}
			productIds = getProductsMappedToIds(productIds, "apple,banana,mango,grapes");
		}
		if (data.toLowerCase().contains("dairy")) {
			if (products.isEmpty()) {
				products = "milk,cheese,yoghurt,butter";
			} else {
				products = products + ",milk,cheese,yoghurt,butter";
			}
			productIds = getProductsMappedToIds(productIds, "milk,cheese,yoghurt,butter");
		}
		if (data.toLowerCase().contains("poultry")) {
			if (products.isEmpty()) {
				products = "eggs,chicken meat,live chickens";
			} else {
				products = products + ",eggs,chicken meat,live chickens";
			}
			productIds = getProductsMappedToIds(productIds, "eggs,chicken meat,live chickens");
		}
		if (data.toLowerCase().contains("meat")) {
			if (products.isEmpty()) {
				products = "chicken meat,goat meat,duck meat,beef";
			} else {
				products = products + ",chicken meat,goat meat,duck meat,beef";
			}
			productIds = getProductsMappedToIds(productIds, "chicken meat,goat meat,duck meat,beef");
		}
		if (data.toLowerCase().contains("pulses")) {
			if (products.isEmpty()) {
				products = "beans,peas,lentil,groundnut";
			} else {
				products = products + ",beans,peas,lentil,groundnut";
			}
			productIds = getProductsMappedToIds(productIds, "beans,peas,lentil,groundnut");
		}
		if (data.toLowerCase().contains("chicken")) {
			if (products.isEmpty()) {
				products = "chicken meat";
			} else {
				products = products + ",chicken meat";
			}
			productIds = getProductsMappedToIds(productIds, "chicken meat");
		}
		if (data.toLowerCase().contains("floriculture") || data.toLowerCase().contains("flower")) {
			if (products.isEmpty()) {
				products = "orchid,marigold,gerbera,rose";
			} else {
				products = products + ",orchid,marigold,gerbera,rose";
			}
			productIds = getProductsMappedToIds(productIds, "orchid,marigold,gerbera,rose");
		}
		if (data.toLowerCase().contains("cereal")) {
			if (products.isEmpty()) {
				products = "maize,oats,barley,rice";
			} else {
				products = products + ",maize,oats,barley,rice";
			}
			productIds = getProductsMappedToIds(productIds, "maize,oats,barley,rice");
		}
		if (data.toLowerCase().contains("millet")) {
			if (products.isEmpty()) {
				products = "millets,pearl millet,finger millet";
			} else {
				products = products + ",millets,pearl millet,finger millet";
			}
			productIds = getProductsMappedToIds(productIds, "millets,pearl millet,finger millet");
		}
		list.add(products);
		list.add(productIds);
		list.add(qualifier);
		return list;
	}

	private String extractQualifiers(String data, ArrayList<String> qualifiers) {
		if (qualifiers != null)
			for (String qualifier : qualifiers) {
				if (data.contains(qualifier))
					return qualifier;
			}
		return null;
	}

	private static String getProductsMappedToIds(String productIds, String productNames) {
		for (String product : productNames.split(",")) {
			if (productIds.isEmpty())
				productIds = product + ":" + baseProducts.get(product);
			else
				productIds = productIds + ";" + product + ":" + baseProducts.get(product);
		}
		return productIds;
	}

	public void getAllBaseProdcuts() {
		try {
			Statement statement = hsqlConnection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM PRODUCTS where variety_id like '%000' and group_id != 44");
			while (resultSet.next()) {
				String productId = resultSet.getString("VSSPSC_CODE");
				String productName = resultSet.getString("UNIVERSAL_NAME").toLowerCase();
				if (productName.trim().length() < 4)
					continue;
				baseProducts.put(productName.toLowerCase().trim(), productId);
				String qualifiersForProduct = resultSet.getString("QUALIFIERS");
				if (qualifiersForProduct != null && !qualifiersForProduct.isEmpty()) {
					ArrayList<String> qualifiersList = new ArrayList<String>();
					for (String productQualifier : qualifiersForProduct.split(",")) {
						if (productQualifier != null && !productQualifier.isEmpty())
							qualifiersList.add(productQualifier.toLowerCase().trim());
					}
					qualifiers.put(productId, qualifiersList);
				}
				String alsoKnowAs = resultSet.getString("ALSO_KNOWN_AS");
				if (alsoKnowAs != null && alsoKnowAs.contains(",")) {
					for (String productOtherName : resultSet.getString("ALSO_KNOWN_AS").split(",")) {
						if (!productOtherName.isEmpty() && productOtherName.length() > 3) {
							baseProducts.put(productOtherName.toLowerCase().trim(), productId);
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	private String removeAllCountryNames(String data) {
		try {
			if (countries.isEmpty()) {
				Statement statement = hsqlConnection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM COUNTRIES");
				while (resultSet.next()) {
					countries.add(resultSet.getString("COUNTRY_NAME").toLowerCase().trim());
				}
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
			}
			for (String country : countries) {
				if (data.toLowerCase().contains(country))
					data = data.toLowerCase().replace(country, " ");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return data;
	}
}