package transaction.script.excelSheet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class DuplicateSheetWriter {
	public static final Logger LOGGER = LoggerFactory.getLogger(DuplicateSheetWriter.class);
	static DB db = null;
	static DBCollection TestUserCollection = null;
	static Gson gson = null;
	static {
		try {
			db = MongoDBUtil.getConnection();
			TestUserCollection = db.getCollection("TestUserCollection");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to connect the database");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Sheet1");
		Row row;

		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		Font font = workbook.createFont();
		font.setColor(IndexedColors.RED.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		int rowid = 0;
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("isDuplicate", true);
		DBCursor cursor = TestUserCollection.find(searchQuery);
		String dbObject = null;
		System.out.println("111111");

		while (cursor.hasNext()) {
			dbObject = cursor.next().toString();
			ExcelTo objExcelTo = gson.fromJson(dbObject, ExcelTo.class);
			System.out.println(gson.toJson(objExcelTo));
			String arr[] = new String[] { objExcelTo.getName(), objExcelTo.getBusinessName(), objExcelTo.getBusinessTypeId(), objExcelTo.getLocationId(), objExcelTo.getMobileNumber(),
					objExcelTo.getIsDuplicate() };
			row = sheet.createRow(rowid++);
			for (int cellcount = 0; cellcount < arr.length; cellcount++) {
				Cell cell = row.createCell(cellcount);
				cell.setCellValue(arr[cellcount]);
				if (cell.getStringCellValue().equalsIgnoreCase("true")) {
					Cell cell1 = row.createCell(cellcount++);
					cell1.setCellValue("Duplicate record");
					cell.setCellStyle(style);
					cellcount = cellcount - 1;
				}
			}
		}
		FileOutputStream fos = new FileOutputStream(new File("/home/vasudhaika/two.xlsx"));
		workbook.write(fos);
		fos.close();
		System.out.println("Done");
	}

}
