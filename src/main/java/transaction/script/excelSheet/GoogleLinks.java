package transaction.script.excelSheet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GoogleLinks {
	public static HashMap<String, String> domains = new HashMap<String, String>();
	public static HashSet<String> ignorelist = new HashSet<String>();
	public static final String GOOGLE_SEARCH_URL = "https://www.google.com/search";

	public static void main(String[] args) throws Exception {
		ignorelist.add("https://www.youtube.com/");
		ignorelist.add("https://www.facebook.com/");
		ignorelist.add("https://twitter.com/");
		ignorelist.add("book");
		FileInputStream fs = new FileInputStream("/home/vasudhaika/test.xlsx");
		Workbook workbookFrom = new XSSFWorkbook(fs);
		Sheet sheetFrom = workbookFrom.getSheetAt(1);
		Sheet sheetdoamins = workbookFrom.createSheet();
		Sheet sheetTo = workbookFrom.createSheet();
		int sheetTorownum=0;
		for (int rowNum = 0; rowNum < 30; rowNum++) {
			Row row = sheetFrom.getRow(rowNum);
			HashMap<String, HashSet<String>> googlelinks = new HashMap<>();
			HashSet<String> matchedUrl = new HashSet<String>();
			String bizName = row.getCell(0).getStringCellValue();
			System.out.println(bizName);
			String email = row.getCell(2).getStringCellValue();
			googlelinks = gettingLinks(matchedUrl, bizName, email);
			System.out.println(matchedUrl.size());
			System.out.println(googlelinks.size());
			gettingDomains(googlelinks);
			System.out.println(domains.size());
			sheetTorownum=writingToExcelsLinks(bizName, email, sheetTo, googlelinks, matchedUrl,sheetTorownum);
			writingToExcelsDomains(sheetdoamins);
		}
		FileOutputStream fileOutputStream = new FileOutputStream("/home/vasudhaika/test.xlsx");
		workbookFrom.write(fileOutputStream);
	}

	public static HashMap<String, HashSet<String>> gettingLinks(HashSet<String> matchedUrl, String... searchParameters) throws Exception {
		HashMap<String, HashSet<String>> googlelinks = new HashMap<>();
		HashSet<String> linklist = new HashSet<String>();
		boolean isIgnoredurl = false;
		boolean linkMatch = false;
		String key = "";
		for (String searchParamter : searchParameters) {
			key = key + searchParamter;
			System.out.println(searchParamter);
			String searchURL = GOOGLE_SEARCH_URL + "?q=" + searchParamter + "&num=" + 20;
			// without proper User-Agent, we will get 403 error
			System.out.println(searchURL);
			Document doc = Jsoup.connect(searchURL).userAgent("Mozilla/5.0 Chrome/19.0.1042.0").timeout(60000).followRedirects(true).get();
			// If google search results HTML change the <h3 class="r" to <h3
			// class="r1"
			// we need to change below accordingly
			Elements results = doc.select("h3.r > a");
			for (Element result : results) {
				String linkHref = result.attr("href");
				String linkhref1 = linkHref.substring(7, linkHref.indexOf("&")).trim();
				System.out.println(linkhref1);
				for (String ignoredurl : ignorelist) {
					if (linkhref1.contains(ignoredurl)) {
						isIgnoredurl = true;
						break;
					}
				}
				if (isIgnoredurl) {
					isIgnoredurl = false;
				} else {
					for (Object entry : linklist) {
						if (entry.equals(linkhref1))
							linkMatch = true;
					}
					linklist.add(linkhref1);
					if (linkMatch) {
						matchedUrl.add(linkhref1);
					}
				}
			}
		}

		googlelinks.put(key, linklist);
		System.out.println(googlelinks.get(key).size());
		return googlelinks;

	}

	public static void gettingDomains(HashMap<String, HashSet<String>> googlelinks) {
		for (String key : googlelinks.keySet()) {
			HashSet<String> linklist = googlelinks.get(key);
			for (String link : linklist) {
				int count = 0;
				if (link.startsWith("http://") || link.startsWith("https://")) {
					String templurl = link.substring(link.indexOf("://") + 3);
					String domainLink = templurl.substring(0, templurl.indexOf("/"));
					if (domains.get(domainLink) != null && !domains.get(domainLink).isEmpty()) {
						count = Integer.parseInt(domains.get(domainLink));
						count = count + 1;
						domains.put(domainLink, Integer.toString(count));
					} else {
						domains.put(domainLink, "1");
					}
				}
			}

		}
	}

	public static int writingToExcelsLinks(String bizName, String email, Sheet sheetTo, HashMap<String, HashSet<String>> googlelinks, HashSet<String> matchedUrl,int rownum) {
		
		HashSet<String> googleUrls=new HashSet<String>();
		for(String key:googlelinks.keySet()){
			googleUrls=googlelinks.get(key);
		}
		int rowNum=rownum;
		int i=0;
		for(String url:googleUrls){
			Row row = sheetTo.createRow(rowNum);
			if(i==0){
			row.createCell(0).setCellValue(bizName);
			row.createCell(1).setCellValue(email);
			}
			row.createCell(3).setCellValue(url);
			rowNum++;
			i++;
		}
		return rowNum;
	}
	public static void writingToExcelsDomains(Sheet sheetdoamins) {
		
		int i=0;
		for(String domainName:domains.keySet()){
			Row row = sheetdoamins.createRow(i);
			row.createCell(0).setCellValue(domainName);
			row.createCell(1).setCellValue(domains.get(domainName));
			i++;
		}
		
	}
}
